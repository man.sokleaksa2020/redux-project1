import React from "react";
import "./App.css";
import Articles from "./views/Articles";
import Home from "./views/Home";

export default function App() {
  return <Articles />;
}
