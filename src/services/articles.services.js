import { API } from "./API"

export const fetch_all_articles = async() => {
    try {
        const result = await API.get("/articles")
        console.log("fetch_all_articles:", result.data.data)
        return result.data.data
    } catch (error) {
        console.log("fetch_all_articles error:", error);
    }
}