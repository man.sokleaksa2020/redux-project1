import axios from "axios"
import { fetch_all_articles } from "../../services/articles.services"

export const FETCH_ALL_ARTICLES = "FETCH_ALL_ARTICLES"

export const fetchAllArticles = () => {
    return async dispatch => {
        const result = await fetch_all_articles()
        dispatch({
            type: FETCH_ALL_ARTICLES,
            articles: result,
            isLoading: false,
        })
    }
}