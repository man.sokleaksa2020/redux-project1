export const INCREASE = "INCREASE"
export const DECREASE = "DECREASE"


export const increase = (user) => {
    return{
        type: INCREASE,
        user: user
    }
}


export const decrease = () => {
    return{
        type: DECREASE,
    }
}