import { applyMiddleware, combineReducers, createStore } from "redux";
import articlesReducer from "../reducers/articlesReducer";
import counterReducer from "../reducers/counterReducer";
import thunk from 'redux-thunk'
import logger from 'redux-logger'

const rootReducers = combineReducers({
    counterReducer,
    articlesReducer
})

export const store = createStore(rootReducers, applyMiddleware(thunk, logger))