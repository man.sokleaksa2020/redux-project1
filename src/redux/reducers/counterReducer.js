import { DECREASE, INCREASE } from "../actions/counterAction";

const initState = {
    counter: 100,
    user: "seng"
}

export default function counterReducer(state = initState, action){
    console.log("ACTION:", action);
    switch(action.type){
        case INCREASE:
            return{
                ...state,
                counter: state.counter + 1,
                user: action.user
            }
        case DECREASE:
            return{
                ...state,
                counter: state.counter - 1
            }
        default:
            return state
    }
}