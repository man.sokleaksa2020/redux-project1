import { FETCH_ALL_ARTICLES } from "../actions/articlesAction"

const initState = {
    articles: [],
    isLoading: true,
}

export default function articlesReducer(state = initState, action){
    console.log("ACTION:", action);
    switch(action.type){
        case FETCH_ALL_ARTICLES:
            return{
               ...state,
               articles: action.articles,
               isLoading: action.isLoading
            }
        default:
            return state
    }
}
